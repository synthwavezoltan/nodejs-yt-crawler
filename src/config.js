module.exports = {
    "TEXTFILE_PATH": "./links.txt",
    "API_KEY": process.env.YT_API_KEY,
    "DATABASE": {
        host: 'localhost',
        port: process.env.DB_PORT,
        user: process.env.DB_USER,
        password: process.env.DB_ROOT_PW,
        database: process.env.DB_NAME,
    
        multipleStatements : true,
    }
}