const request = require('request');

const config = require('./config');

let defaultGetRoute = function(req, res, next) {
	console.log(req.originalUrl)
	res.send('you requested ' + req.originalUrl)
}

let getRelatedVideosUrl = function (videoid) {
    return 'https://www.googleapis.com/youtube/v3/search?'
        + 'part=snippet'
        + '&maxResults=50'
            //+ '&q=' + keyword
        + '&relatedToVideoId=' + videoid
        + '&type=video'
        + '&videoDefinition=any'
        + '&videoDuration=any'
        + '&key=' + config.API_KEY;
}

let getVideoDataUrl = function(id) {
    return 'https://www.googleapis.com/youtube/v3/videos'
        + '?part=snippet'
        + '&id=' + id
        + '&key=' + config.API_KEY;
}

let get = function(url, callback, next) {
    request(url, (err, res, body) => {
        let code = res.statusCode

        if((err || code != 200) && typeof res != 'undefined') {
            //res.send({
            //    code: code,
            //    err: err
            //});

            if (next) {
                return next();
            }

            return;
        }

        callback(body, res)

        if (next) {
            return next();
        }

        return;
    });
}

let getRelatedVideosById = function(videoid, callback) {
    get(getRelatedVideosUrl(videoid), callback)
}

module.exports = {
	"defaultGetRoute": defaultGetRoute,
	"getRelatedVideosUrl": getRelatedVideosUrl,
	"get": get,
	"getRelatedVideosById": getRelatedVideosById,
	"getVideoDataUrl": getVideoDataUrl
}