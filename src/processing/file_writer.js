/**
 * Created by Katamori on 2016.02.18..
 */

const request = require('request');
const fs = require('fs');
const readline = require('readline');

const config = require('../config');
const utils = require('../utils');

let task_getRelated = function(videoId) {
    let id = videoId.substr(videoId.length-11, videoId.length-1)

    utils.getRelatedVideosById(id, function(body, response, videoid, input) {
        let output = loadData_old(body, videoid, input.task);

        res.send(output);
    })
}

// QUERY
// API key is associated to the "Doom video database" project of "zol.sch93@gmail.com" account
let getRelated_old = function(input, callback) {
    task_getRelated(input.vid_id, callback)
};

let expand_old = function(input) {
    task_expand(input.it)
}

let task_expand = function(iteration) {
    var videoid = "";

    // closure to count lines; source: http://www.w3schools.com/js/js_function_closures.asp
    var asyncCounter = (() => {
        var count = 0;
        return () => count += 1
    })();

    //ReadLine goes into the picture here; a basic reader that exits if "iteration"-th line is found
    readline.createInterface({
        input: fs.createReadStream(config.TEXTFILE_PATH)
    }).on('line', line => {
        var lineNumber = asyncCounter();

        if (iteration == lineNumber - 1) {
            videoid = line;
            this.close();
        };
    }).on('close', () => {
        videoid = videoid.substr(0, 11);

        utils.getRelatedVideosById(videoid, function(body, response, videoid, input) {
            let output = loadData_old(body, videoid, input.task);
    
            res.send(output);
        })
    });
}

// callback, that does the job
// input is the response to the AJAX request above
// source: http://stackoverflow.com/a/14220323/2320153
let loadData_old = function(responseData, sourcevid, taskName) {
    var result = JSON.parse(responseData);

    for(i = 0; i < result.items.length - 1; i++) {
        var redundant = false; //redundancy check
        var keywordMatch = false; //keyword check

        keywordMatch = keyword.length > 0 
            && result.items[i].snippet.title.toLowerCase().includes(
                keyword.toLowerCase()
            )

        if(!(!redundant && keywordMatch)) {
            continue;
        }

        fs.appendFile(config.TEXTFILE_PATH,
            result.items[i].id.videoId + "\r\n",
            err => {
                if (err) throw err
            }
        );
    };

    return {
        dat: result,
        gottentask: taskName,
    };

};

module.exports = {
    "getRelated_old": getRelated_old,
    "expand_old": expand_old,
    "loadData_old": loadData_old
}