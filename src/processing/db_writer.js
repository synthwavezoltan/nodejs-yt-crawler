/**
 * Created by Katamori on 2016.03.24..
 */

const request = require('request');
const async = require ('async');

const mysql = require('mysql');

const config = require('../config');
const utils = require('../utils');

var connection = mysql.createConnection(config.DATABASE);

var HOSTNAME = "localhost"

 // "DISPLAY_DB" FUNCTIONALITY
let getAll = function(expressResponse) {
    try {
        connection.connect();

        connection.query('SELECT * FROM videos', function(err, rows, fields) {
            if (err) throw err;

            expressResponse.send(rows)

            connection.end();
        })
    } catch (ex) {
        console.log(ex.code)
        console.log(ex.message)

        expressResponse.send(ex)
    }
};

let tryNewChannel = function(result, parsed_body) {
    if (result.affectedRows != 0) return

    let some_snippet = parsed_body.items[0].snippet;

    var chanelId =some_snippet.channelId;
    var channelTitle = some_snippet.channelTitle.replace("'", "").substr(0,250)

    var anotherSQLstr =
    "INSERT INTO users (id, name) VALUES ('"+chanelId+"', '"+channelTitle+"')";

    connection.query(anotherSQLstr, function(err, result) {
        if (err) {
            switch (err.code) {
                case "ER_DUP_ENTRY":
                    console.log("Duplication: " + channelTitle)
                    return
                    case "ER_TRUNCATED_WRONG_VALUE_FOR_FIELD":
                        console.log("Got emoji of some other sh*t in title: " + channelTitle)
                        return
                default:
                    console.log(err.code)
                    throw err;
            }
        }

        console.log("New channel added: " + channelTitle)
    });
}

/* AUTOCOMPLETE EXAMPLE 3: ADD UPLOADER IF IT EXISTS; WRITE IF IT DOESN'T */
let addOrWriteUploader = function(body, row, connection) {
    jsonData = JSON.parse(body)

    if(typeof jsonData.items[0] == "undefined") return;

    var channelId = jsonData.items[0].snippet.channelId.replace(/'/g, "");;

    var autocSQLstr =
        "update videos "+
        "inner join users on users.id='" + channelId + "' " +
        "set videos.uploader = users.id " +
        "where users.id is not null " +
        "and videos.id='"+row.id+"'";

    connection.query(autocSQLstr, function(err, result) {
        if (err) throw err;

        // professional logging <3
        if (row.num_id % 70 < 5){
            console.log(
                row.num_id+"|"+
                jsonData.items[0].snippet.title+"|"+
                jsonData.items[0].snippet.channelTitle);
        };

        //try to add a new channel to the existing list
        tryNewChannel(result, jsonData)
    });
}



// "AUTOCOMPLETE" FUNCTIONALITY
let writeVideoData = function() {
    connection.connect();

    let query = 'SELECT id, related_dst FROM videos where uploader IS NULL order by related_dst';

    connection.query(query, function(err, rows, fields) {
        if (err) throw err;

        //using "async" npm was not my idea; source: http://stackoverflow.com/a/36385121/2320153
        async.eachSeries(rows, function(row, next){
            let videoDataUrl = utils.getVideoDataUrl(row.id)

            utils.get(videoDataUrl, function(body, response) {
                addOrWriteUploader(body, row, connection);
            }, next)
        });
    });
};

let addNewVideo = function(videoData, parentVideo) {
    var SQLstring =
        "UPDATE videos SET checked = 1 "
        +"WHERE id='" + parentVideo.id  + "'; " +

        "INSERT INTO videos (id, title, related_dst) "
        +"VALUES ("
        +"'" + videoData.id +"',"
        +"'" + videoData.title +"',"
        +"'" + videoData.relatedDistance +"'); "

        "update videos "+
        "inner join users on users.id='" + videoData.channelId + "' " +
        "set videos.uploader = users.id " +
        "where users.id is not null " +
        "and videos.id='"+parentVideo.id+"'";

    connection.query(SQLstring, function(err, result) {
        if (err) {
            switch (err.code) {
                case "ER_DUP_ENTRY":
                    //console.log("Duplication")
                    return
                    case "ER_TRUNCATED_WRONG_VALUE_FOR_FIELD":
                        console.log("Got emoji of some other sh*t in title")
                        return
                default:
                    console.log(err.code)
                    throw err;
            }
        }

        console.log(parentVideo.id + " | NEW RELATED VID: "+videoData.id+" -- "+videoData.title);
    });
}

let parseRelatedVideos = function(relatedVideosData, row) {
    if (typeof relatedVideosData.items == "undefined") return

   // let keyword = ""

    for(i=1;i<relatedVideosData.items.length-1;i++){
        if (!(
            typeof relatedVideosData.items[i] != "undefined" 
            //&& keyword.length > 0 
            //&& relatedVideosData.items[i].snippet.title
            //    .toLowerCase()
            //    .includes(keyword.toLowerCase())
            ))
        {
            continue
        }

        let item = relatedVideosData.items[i]
        let data = {
            "id": item.id.videoId,
            "title": item.snippet.title.replace(/'/g, "").replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, ''),
            "channelId": item.snippet.channelId,
            "relatedDistance": (row.related_dst) + 1,
        }

        addNewVideo(data, row)
    };
}

let getNotExpandedVideosSql = function() {
    return "SELECT * " +
    "FROM videos " +
    "where subject > 0 and subject <> 50" +
    "AND checked = 0 " +
    "ORDER BY related_dst, id";
}

// "EXPAND" FUNCTIONALITY (WITH CALLBACK)
let task_expand = function(expressResponse) {
    connection.connect();

    let expandSQLstring = getNotExpandedVideosSql()
    console.log(expandSQLstring)
    connection.query(expandSQLstring, function(err, notExpandedVideos, fields) {
        if (err) throw err;

        //using "async" npm was not my idea; source: http://stackoverflow.com/a/36385121/2320153
        async.eachSeries(notExpandedVideos, function(videoData, next){
            let relatedVideosUrl = utils.getRelatedVideosUrl(videoData.id)

            utils.get(relatedVideosUrl, function(body, response) {
                parseRelatedVideos(JSON.parse(body), videoData)
            }, next);
        });
     });
}

module.exports = {
    "getDatabase": getAll,
    "writeVideoData": writeVideoData,
    "expand": task_expand,
}







