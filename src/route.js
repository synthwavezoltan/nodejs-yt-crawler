const express = require('express');

var databaseWriter = require("./processing/db_writer.js");
var fileWriter = require("./processing/file_writer.js");
const utils = require('./utils');

var router = express.Router();

// todo put it somewhere:
// res.setHeader('Content-Type', 'application/json');

/* GET */
router.get('/', utils.defaultGetRoute);

router.get('/api', utils.defaultGetRoute);

router.get('/api/display_db', (req, res) => databaseWriter.getDatabase(res));

/* POST */

router.post('/api/job/expand', (req, res) => databaseWriter.expand());

router.post('/api/job/autocomplete_db', (req, res) => databaseWriter.writeVideoData());

/* POST /api/getrelated [body:keyword] */
router.post('/api/old/job/getrelated', (req, res) => fileWriter.getRelated_old(req.body, fileWriter.loadData_old));

/* POST /api/getrelated [body:it] */
router.post('/api/old/job/expand', (req, res) => fileWriter.getRelated_old(req.body));

module.exports = router;