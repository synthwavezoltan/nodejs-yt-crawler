/* -------------------------------------------------------------------------------------------------- */
/* -----------------------------------THE OLD METHOD------------------------------------------------- */
/* -------------------------------------------------------------------------------------------------- */
/* ----------------------(originally wrote links into a txt file)------------------------------------ */
/* ---------------------(kept for historical and tutorial reasons)----------------------------------- */
/* -------------------------------------------------------------------------------------------------- */

/*

 case "getrelated":

 //first aka initial request; sends the chosen link to server
 GetRelated.YTAPP_GetRelated(
 req.body,
 res,
 GetRelated.YTAPP_Callback_LoadData);

 break;

 case "expand":

 //a JSON to store the input value a bit differently
 //task and vid_id are unnecessary
 var new_input = {
 task: "expand",
 keyword: req.body.keyword,
 it: req.body.it
 };

 GetRelated.YTAPP_GetRelated(
 new_input,
 res,
 GetRelated.YTAPP_Callback_LoadData);
 break;

 */