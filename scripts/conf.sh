#!/bin/bash

cd $PWD/devops

case $1 in
    start)
        docker-compose up -d
    ;;

    enter)
        docker-compose exec db bash
    ;;

    inspect)
        docker-compose ps
    ;;

    stop)
        docker-compose down
    ;;

    *)

    ;;
esac
